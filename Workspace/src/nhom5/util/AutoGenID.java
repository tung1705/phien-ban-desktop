
package nhom5.util;

import java.util.HashMap;
import nhom5.model.ChiPhi;
import nhom5.model.CuaHang;
import nhom5.model.HoaDon;
import nhom5.model.HoaDonLuong;
import nhom5.model.NhanVienBanHang;
import nhom5.model.SanPham;

public class AutoGenID {
    public static int sinhMaHD(){
        HashMap<Integer,HoaDon> dsHD = CuaHang.getDanhSachHoaDon();
        int cnt = dsHD.size();
      
        return cnt+1;
    }
    
    public static int sinhMaHDL(){
        HashMap<Integer,HoaDonLuong> dsHDL = CuaHang.getDanhSachBangLuong();
        int cnt = dsHDL.size();
        
        return cnt+1;
    }
    
    public static int sinhMaSP(){
        HashMap<Integer,SanPham> dsSP = CuaHang.getDanhSachSanPham();
        
        int cnt = dsSP.size();
       
        return cnt+1;
    }
    
    public static int sinhMaNV(){
        HashMap<Integer,NhanVienBanHang> dsNV = CuaHang.getDanhSachNhanVien();
        
        int cnt = dsNV.size();
        
        return cnt+1;
    }
    
    public static int sinhMaCP(){
        HashMap<Integer,ChiPhi> dsCP = CuaHang.getDanhSachChiPhi();
        int cnt = dsCP.size();
        
        return cnt+1;
    }
}
