-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 27, 2017 lúc 02:45 PM
-- Phiên bản máy phục vụ: 10.1.28-MariaDB
-- Phiên bản PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `nmcnpm`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `donghoadonluong`
--

CREATE TABLE `donghoadonluong` (
  `manhanvien` varchar(11) COLLATE utf8_vietnamese_ci NOT NULL,
  `tennhanvien` text COLLATE utf8_vietnamese_ci NOT NULL,
  `giotichluy` int(11) NOT NULL,
  `luongcoban` int(11) NOT NULL,
  `mahdl` varchar(11) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `donghoadonluong`
--
ALTER TABLE `donghoadonluong`
  ADD PRIMARY KEY (`manhanvien`,`mahdl`),
  ADD KEY `fk_dhdl` (`mahdl`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `donghoadonluong`
--
ALTER TABLE `donghoadonluong`
  ADD CONSTRAINT `fk_dhdl` FOREIGN KEY (`mahdl`) REFERENCES `hoadonluong` (`mahdl`),
  ADD CONSTRAINT `fk_dhdl1` FOREIGN KEY (`manhanvien`) REFERENCES `nhanvien` (`manhanvien`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
