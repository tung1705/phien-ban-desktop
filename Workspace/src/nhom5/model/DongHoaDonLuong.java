/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhom5.model;

import java.io.Serializable;

/**
 *
 * @author krayz
 */
public class DongHoaDonLuong implements Serializable{
    private NhanVienBanHang nhanVien;
    private int luong;
    
    public DongHoaDonLuong(){
        super();
    }

	public DongHoaDonLuong(NhanVienBanHang nhanVien, int luong) {
		super();
		this.nhanVien = nhanVien;
		this.luong = luong;
	}
        
        public DongHoaDonLuong(NhanVienBanHang nhanVien) {
		super();
		this.nhanVien = nhanVien;
		this.luong = nhanVien.getLuong();
	}

	public NhanVienBanHang getNhanVien() {
		return nhanVien;
	}

	public void setNhanVien(NhanVienBanHang nhanVien) {
		this.nhanVien = nhanVien;
	}

	public int getLuong() {
		return luong;
	}

	public void setLuong(int luong) {
		this.luong = luong;
	}

	
    
}
