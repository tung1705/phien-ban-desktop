package csdl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class TuongTacCSDL {

    public static Connection conn = createConnection();

    // Ket noi toi server
    public static Connection createConnection() {

        try {

            String strConnection = "jdbc:mysql://localhost:3306/test";
            Properties prop = new Properties();
            prop.put("user", "root");
            prop.put("password", "");
            Driver driver = new Driver();
            conn = (Connection) driver.connect(strConnection, prop);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }

    // Truy van cau truy van
    public static ResultSet guiTruyVan(MyPreparedStatement cauLenh) {
        
        try {
            if (conn != null) {
                Statement statement = (Statement) conn.createStatement();
                ResultSet result = statement.executeQuery(cauLenh.toString());
                return result;
            } else {
                JOptionPane.showMessageDialog(null, "Khong the ket noi den sql server de cap nhat thay doi");
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Cap nhat du lieu 
    public static void guiCapNhat(MyPreparedStatement cauLenh) {
        try {

            if (conn != null) {

                Statement statement = (Statement) conn.createStatement();

                if (statement.executeUpdate(cauLenh.toString()) <= 0) {
                    JOptionPane.showMessageDialog(null, "Khong the thuc hien cau lenh: " + cauLenh);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Khong the ket noi den sql server de cap nhat thay doi");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
