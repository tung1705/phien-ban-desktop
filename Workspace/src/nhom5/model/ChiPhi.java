package nhom5.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

public class ChiPhi implements Serializable{
        
    private int id;
	private int soTien;
	private String lyDo;
	private LocalDate ngay;
        
    public int getMaCP() {
         return id;
    }

    public void setMaCP(int maCP) {
        this.id = maCP;
    }
        
	public int getSoTien() {
		return soTien;
	}
	public void setSoTien(int soTien) {
		this.soTien = soTien;
	}
	public String getLyDo() {
		return lyDo;
	}
	public void setLyDo(String lyDo) {
		this.lyDo = lyDo;
	}
	
        public LocalDate getNgay()
        {
            return this.ngay;
        }
        
        public void setNgay(LocalDate ngay)
        {
            this.ngay = ngay;
        }
        
        public ChiPhi(){
            super();
        }
        
	public ChiPhi(int ma, int soTien, String lyDo, LocalDate ngay) {
		super();
        this.id = ma;
		this.soTien = soTien;
		this.lyDo = lyDo;
		this.ngay = ngay;
	}
	
}
