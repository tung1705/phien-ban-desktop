package nhom5.model;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import csdl.MyPreparedStatement;
import csdl.TuongTacCSDL;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;

public class QuanLy implements Serializable {

    private static int id;
    private static String taiKhoan;
    private static String matKhau;

    public static int getId() {
		return id;
	}

	public static void setId(int id) {
		QuanLy.id = id;
	}

	public static String getTaiKhoan() {
		return taiKhoan;
	}
	
	public static String getMatKhau() {
		return matKhau;
	}

	public static void docDuLieu() {
        try{
            String sql = "select * from quanly";
            MyPreparedStatement sm = new MyPreparedStatement(sql);
            ResultSet res = truyVan(sm);
            if(res!=null && res.next()){
            	QuanLy.id = res.getInt(1);
                QuanLy.taiKhoan = res.getString(2);
                QuanLy.matKhau = res.getString(3);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return;
    }

    public static void capNhatTaiKhoan(String taiKhoan) {
    	QuanLy.taiKhoan = taiKhoan;
        try{
            String sql = "update quanly set taikhoan=? where id=?";
            MyPreparedStatement sm = new MyPreparedStatement(sql);
            sm.setValue(1, taiKhoan);
            sm.setValue(2, QuanLy.id);
            capNhat(sm);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void capNhatMatKhau(String matKhau) {
    	QuanLy.matKhau = matKhau;
        try{
            String sql = "update quanly set matkhau=? where id=?";
            MyPreparedStatement sm = new MyPreparedStatement(sql);
            sm.setValue(1, matKhau);
            sm.setValue(2, QuanLy.id);
            capNhat(sm);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    // Cac phuong thuc quan ly nhan vien
    public static void themNhanVien(NhanVienBanHang nv) {
    	CuaHang.getDanhSachNhanVien().put(new Integer(nv.getMa()), nv);

        String sql = "insert into nhanvien(ten, matkhau, luong, ngaysinh, ngayvaolam) values(?,?,?,?,?)";
        MyPreparedStatement statement = new MyPreparedStatement(sql);
        statement.setValue(1, nv.getTen());
        statement.setValue(2, nv.getMatKhau());
        statement.setValue(3, nv.getLuong());
        statement.setValue(4, nv.getNgaySinh());
        statement.setValue(5, nv.getNgayVaoLam());
 
        capNhat(statement);

    }

    public static void xoaNhanVien(int id) {
    	CuaHang.getDanhSachNhanVien().remove(id);
    	
        MyPreparedStatement statement = new MyPreparedStatement("delete from nhanvien where id=?");
        statement.setValue(1, id);
        capNhat(statement);
    }

    public static void thayDoiThongTin(int id, String ten, String matKhau, int luong, LocalDate ngaySinh, LocalDate ngayVaoLam) {
    	NhanVienBanHang nv = CuaHang.getDanhSachNhanVien().get(id);
    	nv.setTen(ten);
    	nv.setMatKhau(matKhau);
    	nv.setLuong(luong);
    	nv.setNgaySinh(ngaySinh);
    	nv.setNgayVaoLam(ngayVaoLam);
        String sql = "update nhanvien set ten=?,matkhau=?,luong=?,ngaysinh=?,ngayvaolam=? where id=?";

        MyPreparedStatement statement = new MyPreparedStatement(sql);
        statement.setValue(1, ten);
        statement.setValue(2, matKhau);
        statement.setValue(3, luong);
        statement.setValue(4, ngaySinh);
        statement.setValue(5, ngayVaoLam);
        statement.setValue(6, id);

        capNhat(statement);
    }

    public static int traLuong(NhanVienBanHang nv) {

        if(nv!=null){
            int luong = nv.getLuong();

            
            return luong;
        }
        else{
            return 0;
        }
    }
    
    // Cac phuong thuc quan ly san pham
    public static void themSanPham(SanPham sp) {
    	CuaHang.getDanhSachSanPham().put(new Integer(sp.getId()), sp);
        String sql = "insert into SanPham(loai, ten, giamua, giaban, conlai, daban, likes, linkanh, congtyphathanh, nxb, tacgia, dichgia, loaibia, sotrang, ngayxuatban, sku, noidungchinh)"
        		+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        MyPreparedStatement sm = new MyPreparedStatement(sql);
        sm.setValue(1, sp.getLoai());
        sm.setValue(2, sp.getTen());
        sm.setValue(3, sp.getGiaMua());
        sm.setValue(4, sp.getGiaBan());
        sm.setValue(5, sp.getConLai());
        sm.setValue(6, sp.getDaBan());
        sm.setValue(7, sp.getLikes());
        sm.setValue(8, sp.getLinkanh());
        sm.setValue(9, sp.getCongTyPhatHanh());
        sm.setValue(10, sp.getNhaXuatBan());
        sm.setValue(11, sp.getTacGia());
        sm.setValue(12, sp.getDichGia());
        sm.setValue(13, sp.getLoaiBia());
        sm.setValue(14, sp.getSoTrang());
        sm.setValue(15, sp.getNgayXuatBan());
        sm.setValue(16, sp.getSku());
        sm.setValue(17, sp.getNoiDungChinh());
        
        capNhat(sm);
    }

    public static void xoaSanPham(int id) {
    	CuaHang.getDanhSachSanPham().remove(id);
        String sql = "delete from SanPham where id=?";
        MyPreparedStatement sm = new MyPreparedStatement(sql);
        sm.setValue(1, id);
        capNhat(sm);
    }

    public static void capNhatSanPham(int id, String loai, String ten, int giaMua, int giaBan, int conLai, int daBan, int likes, String linkanh,
			String congTyPhatHanh, String nhaXuatBan, String tacGia, String dichGia, String loaiBia, int soTrang,
			String ngayXuatBan, String sku, String noiDungChinh) {
    	
    	SanPham sp = CuaHang.getDanhSachSanPham().get(id);
    	sp.setLoai(loai);
    	sp.setTen(ten);
    	sp.setGiaMua(giaMua);
    	sp.setGiaBan(giaBan);
    	sp.setConLai(conLai);
    	sp.setDaBan(daBan);
    	sp.setLikes(likes);
    	sp.setLinkanh(linkanh);
    	sp.setCongTyPhatHanh(congTyPhatHanh);
    	sp.setNhaXuatBan(nhaXuatBan);
    	sp.setTacGia(tacGia);
    	sp.setDichGia(dichGia);
    	sp.setLoaiBia(loaiBia);
    	sp.setSoTrang(soTrang);
    	sp.setNgayXuatBan(ngayXuatBan);
    	sp.setSku(sku);
    	sp.setNoiDungChinh(noiDungChinh);
        String sql = "update SanPham set loai=?,ten=?,giamua=?,giaban=?,conlai=?,daban=?,likes=?,linkanh=?,congtyphathanh=?,nxb=?,tacgia=?,"
        		+ "dichgia=?,loaibia=?,sotrang=?,ngayxuatban=?,sku=?,noidungchinh=? where id=?";
        MyPreparedStatement sm = new MyPreparedStatement(sql);
        sm.setValue(1, loai);
        sm.setValue(2, ten);
        sm.setValue(3, giaMua);
        sm.setValue(4, giaBan);
        sm.setValue(5, conLai);
        sm.setValue(6, daBan);
        sm.setValue(7, likes);
        sm.setValue(8, linkanh);
        sm.setValue(9, congTyPhatHanh);
        sm.setValue(10, nhaXuatBan);
        sm.setValue(11, tacGia);
        sm.setValue(12, dichGia);
        sm.setValue(13, loaiBia);
        sm.setValue(14, soTrang);
        sm.setValue(15, ngayXuatBan);
        sm.setValue(16, sku);
        sm.setValue(17, noiDungChinh);
        sm.setValue(18, id);
        
        capNhat(sm);
    }

    // Cac phuong thuc quan ly hoa don
    
    public static void themHoaDon(HoaDon hd) {
    	CuaHang.getDanhSachHoaDon().put(new Integer(hd.getId()), hd);
    	String sql = "insert into hoadon(id, kh_id, nv_id, ngay) values(?,?,?,?)";
        MyPreparedStatement sm1 = new MyPreparedStatement(sql);
        sm1.setValue(1, hd.getId());
        sm1.setValue(2, hd.getKhachHangId());
        sm1.setValue(3, hd.getNhanVien().getMa());
        sm1.setValue(4, hd.getNgay());
        capNhat(sm1);
        
        for(DongHoaDon dong : hd.getBanGhi()){
            String sql1 = "insert into donghoadon(hd_id, sp_id, giamua, giaban, soluong) values(?,?,?,?,?)";
            MyPreparedStatement sm = new MyPreparedStatement(sql1);
            sm.setValue(1,hd.getId());
            sm.setValue(2,dong.getSanPham().getId());
            sm.setValue(3,dong.getGiaMua());
            sm.setValue(4,dong.getGiaBan());
            sm.setValue(5,dong.getSoLuong());
            capNhat(sm);
        }
    }

    public static int thongKeDoanhThu(int batDau, int ketThuc) {
        int doanhThu = 0;
        for (HoaDon hd : CuaHang.getDanhSachHoaDon().values()) {
            if (hd.getNgay().getMonthValue() >= batDau && hd.getNgay().getMonthValue() <= ketThuc) {
                doanhThu += hd.tinhTongTien();
            }
        }
        return doanhThu;
    }

    public static int thongKeChiPhi(int batDau, int ketThuc) {
        int chiPhi = 0;
        for (ChiPhi cp : CuaHang.getDanhSachChiPhi().values()) {
            if (cp.getNgay().getMonthValue() >= batDau && cp.getNgay().getMonthValue() <= ketThuc) {
                chiPhi += cp.getSoTien();
            }
        }

        for (HoaDonLuong hdl : CuaHang.getDanhSachBangLuong().values()) {
            if (hdl.getNgayTraLuong().getMonthValue() >= batDau && hdl.getNgayTraLuong().getMonthValue() <= ketThuc) {
                chiPhi += hdl.tinhTongChi();
            }
        }
        return chiPhi;
    }

    public static int thongKeLoiNhuan(int batDau, int ketThuc) {
        return thongKeDoanhThu(batDau, ketThuc) - thongKeChiPhi(batDau, ketThuc);
    }

    // Cac phuong thuc quan ly chi phi
    public static void themChiPhi(ChiPhi cp) {
    	CuaHang.getDanhSachChiPhi().put(new Integer(cp.getMaCP()), cp);
        String sql = "insert into chiphi(sotien, lydo, ngay) values(?,?,?)";
        MyPreparedStatement sm = new MyPreparedStatement(sql);
        sm.setValue(1, cp.getSoTien());
        sm.setValue(2, cp.getLyDo());
        sm.setValue(3, cp.getNgay());
        capNhat(sm);
    }

    public static void xoaChiPhi(int maCP) {
    	CuaHang.getDanhSachChiPhi().remove(maCP);
        String sql = "delete from chiphi where macp=?";
        MyPreparedStatement sm = new MyPreparedStatement(sql);
        sm.setValue(1, maCP);
        capNhat(sm);
    }

    // Cac phuong thuc quan ly hoa don tien luong
    
    public static void themBangLuong(HoaDonLuong hdl) {
        CuaHang.getDanhSachBangLuong().put(new Integer(hdl.getId()), hdl);
        MyPreparedStatement sm1 = new MyPreparedStatement("insert into hoadonluong(ngaytraluong) values (?)");
        sm1.setValue(1, hdl.getNgayTraLuong());
        capNhat(sm1);
        
        for(DongHoaDonLuong nv : hdl.getBangLuong()){
            String sql = "insert into donghoadonluong(nv_id, hdl_id, sotien) values(?,?,?)";
            MyPreparedStatement sm = new MyPreparedStatement(sql);
            sm.setValue(1,nv.getNhanVien().getMa());
            sm.setValue(2,hdl.getId());
            sm.setValue(3,nv.getLuong());
            capNhat(sm);
        }
    }

    private static void capNhat(MyPreparedStatement statement) {
        TuongTacCSDL.guiCapNhat(statement);
    }

    
    private static ResultSet truyVan(MyPreparedStatement statement) {
        return TuongTacCSDL.guiTruyVan(statement);
    }

}
