package nhom5.util;

import java.sql.Date;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

public class DateTimeFormat {

    private static  SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public static Date parseDate(String input){
        Date date = null;
        try{
            date = new Date(dateFormat.parse(input).getTime());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return date;
    }
    
    public static Timestamp parseTimestamp(String input){
        Timestamp ts = null;
        try{
            ts = new Timestamp(timeStampFormat.parse(input).getTime());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ts;
    }
    
    public static String getDay(Timestamp input){
        return input.toString().substring(0,11);
    }
}