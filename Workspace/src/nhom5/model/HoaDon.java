package nhom5.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;

public class HoaDon implements Serializable{
    private int id;
    private int khachHangId;
    private NhanVienBanHang nhanVien;
    private LocalDate ngay;
    private ArrayList<DongHoaDon> banGhi;


    public HoaDon() {
        super();
        this.banGhi = new ArrayList<DongHoaDon>();
    }
    
    public HoaDon(int id, int khachHangId, NhanVienBanHang nhanVien, LocalDate ngay) {
		super();
		this.id = id;
		this.khachHangId = khachHangId;
		this.nhanVien = nhanVien;
		this.ngay = ngay;
		this.banGhi = new ArrayList<DongHoaDon>();
	}
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public NhanVienBanHang getNhanVien(){
		return this.nhanVien;
	}

	public void setNhanVien(NhanVienBanHang nhanVienId) {
		this.nhanVien = nhanVien;
	}

	public int getKhachHangId() {
		return khachHangId;
	}

	public void setKhachHangId(int khachHangId) {
		this.khachHangId = khachHangId;
	}

	public LocalDate getNgay() {
		return ngay;
	}

	public void setNgay(LocalDate ngay) {
		this.ngay = ngay;
	}

	public ArrayList<DongHoaDon> getBanGhi() {
		return banGhi;
	}

	public void setBanGhi(ArrayList<DongHoaDon> banGhi) {
		this.banGhi = banGhi;
	}

	public void themSanPham(DongHoaDon sp)
    {
        this.banGhi.add(sp);
    }
    public int tinhTongTien()
    {
        int tongTien = 0;
        for(DongHoaDon sp : banGhi)
        {
            tongTien += sp.getSoLuong()*sp.getGiaBan();
        }
        return tongTien;
    }

    public int tinhTongLai()
    {
        int tongLai = 0;
        for(DongHoaDon sp : banGhi)
        {
            tongLai += sp.getSoLuong()*(sp.getGiaBan()-sp.getGiaMua());
        }
        return tongLai;
    }
}
