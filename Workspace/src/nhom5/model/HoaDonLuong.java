package nhom5.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class HoaDonLuong implements Serializable{
    private int id;    
    private LocalDate ngayTraLuong;
    private ArrayList<DongHoaDonLuong> bangLuong; 

    public HoaDonLuong(int maHDL,LocalDate ngayTraLuong) {
        super();
        this.id = maHDL;
        this.ngayTraLuong = ngayTraLuong;
        this.bangLuong = new ArrayList<>();
    }
    
    public HoaDonLuong() {
        super();
        this.bangLuong = new ArrayList<>();
    }
    
    public LocalDate getNgayTraLuong() {
        return ngayTraLuong;
    }

    public void setNgayTraLuong(LocalDate ngayTraLuong) {
        this.ngayTraLuong = ngayTraLuong;
    }

    public ArrayList<DongHoaDonLuong> getBangLuong() {
        return bangLuong;
    }

    public void setLuongNhanVien(ArrayList<DongHoaDonLuong> bangLuong) {
        this.bangLuong = bangLuong;
    }	
    
    public int getId() {
        return id;
    }

    public void setId(int maHDL) {
        this.id = maHDL;
    }
    
    public void themNV(DongHoaDonLuong nv){
        bangLuong.add(nv);
    }
    public int tinhTongChi(){
        int tongChi = 0;
        for(DongHoaDonLuong nv : bangLuong){
            tongChi += nv.getLuong();
        }
        return tongChi;
    }
}
