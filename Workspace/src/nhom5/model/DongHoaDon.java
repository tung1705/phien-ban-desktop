/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nhom5.model;

import java.io.Serializable;

/**
 *
 * @author krayz
 */
public class DongHoaDon implements Serializable{
    
    
    private SanPham sanPham;
    private int giaMua;
    private int giaBan;
    private int soLuong;
    
    public DongHoaDon(){
        super();
    }

	public DongHoaDon(SanPham sp, int soLuong) {
		super();
		this.sanPham = sp;
		this.giaMua = sp.getGiaMua();
		this.giaBan = sp.getGiaBan();
		this.soLuong = soLuong;
	}
	
	public DongHoaDon(SanPham sp, int giaMua, int giaBan, int soLuong) {
		super();
		this.sanPham = sp;
		this.giaMua = giaMua;
		this.giaBan = giaBan;
		this.soLuong = soLuong;
	}

	public SanPham getSanPham() {
		return sanPham;
	}

	public void setSanPham(SanPham sanPham) {
		this.sanPham = sanPham;
	}

	public int getGiaMua() {
		return giaMua;
	}

	public void setGiaMua(int giaMua) {
		this.giaMua = giaMua;
	}

	public int getGiaBan() {
		return giaBan;
	}

	public void setGiaBan(int giaBan) {
		this.giaBan = giaBan;
	}

	public int getSoLuong() {
		return soLuong;
	}

	public void setSoLuong(int soLuong) {
		this.soLuong = soLuong;
	}
    
    
}
