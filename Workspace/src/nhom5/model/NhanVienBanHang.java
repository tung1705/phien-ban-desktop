package nhom5.model;

import csdl.MyPreparedStatement;
import csdl.TuongTacCSDL;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import nhom5.util.AutoGenID;
import java.time.LocalDate;
import java.time.ZoneId;

public class NhanVienBanHang implements Serializable {

    private int id;
    private String ten;
    private String matKhau;
    private int luong;
    private LocalDate ngaySinh;
    private LocalDate ngayVaoLam;

    public NhanVienBanHang(int ma, String matKhau, String ten, int luong, LocalDate ngaySinh, LocalDate ngayVaoLam) {
        super();
        this.id = ma;
        this.ten = ten;
        this.ngaySinh = ngaySinh;
        this.ngayVaoLam = ngayVaoLam;
        this.luong = luong;
        this.matKhau = matKhau;
    }

    public NhanVienBanHang() {
        super();
    }

    public int getMa() {
        return id;
    }

    public void setMa(int ma) {
        this.id = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public LocalDate getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(LocalDate ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public LocalDate getNgayVaoLam() {
        return ngayVaoLam;
    }

    public void setNgayVaoLam(LocalDate ngayVaoLam) {
        this.ngayVaoLam = ngayVaoLam;
    }

    public int getLuong() {
        return luong;
    }

    public void setLuong(int luong) {
        this.luong = luong;
    }

//
//    public void dangNhap() {
//        this.setThoiDiemDangNhap(new Timestamp(new java.util.Date().getTime()));
//        this.setTrangThai(true);
//        
//        String sql = "update nhanvien set thoidiemdangnhap=?,trangthai=? where manhanvien=?";
//        MyPreparedStatement sm = new MyPreparedStatement(sql);
//        sm.setValue(1, this.getThoiDiemDangNhap());
//        sm.setValue(2, this.getTrangThai());
//        sm.setValue(3, this.getMa());
//        TuongTacCSDL.guiCapNhat(sm);
//    }
//
//    public void dangXuat() {
//        if(this.isActive()){
//            Timestamp now = new Timestamp(new java.util.Date().getTime());
//            this.setGioTichLuy(now.getHours() - this.getThoiDiemDangNhap().getHours()+this.getGioTichLuy());
//            this.setTrangThai(false);
//            
//            String sql = "update nhanvien set trangthai=?,giotichluy=? where manhanvien=?";
//            MyPreparedStatement sm = new MyPreparedStatement(sql);
//            sm.setValue(1, this.getTrangThai());
//            sm.setValue(2, this.getGioTichLuy());
//            sm.setValue(3, this.getMa());
//            TuongTacCSDL.guiCapNhat(sm);
//        }
//    }

    public HoaDon thanhToan(HashMap<Integer, Integer> dsBan) {
    	
    	LocalDate now = LocalDate.now( ZoneId.of( "Asia/Ho_Chi_Minh" ) );
        HoaDon hoaDon = new HoaDon(AutoGenID.sinhMaHD(), 2, this, now);
        for (Integer maSP : dsBan.keySet()) {

            SanPham sp = CuaHang.getDanhSachSanPham().get(maSP);
            int soLuong = dsBan.get(maSP);
            DongHoaDon clone = new DongHoaDon(sp, soLuong);
            hoaDon.themSanPham(clone);
            sp.setConLai(sp.getConLai() - soLuong);
            sp.setDaBan(sp.getDaBan() + soLuong);

            String sql = "update SanPham set conlai=?, daban=? where id=?";
            MyPreparedStatement sm = new MyPreparedStatement(sql);
            sm.setValue(1, sp.getConLai());
            sm.setValue(2, sp.getDaBan());
            sm.setValue(3, maSP);
            TuongTacCSDL.guiCapNhat(sm);
            
        }
        return hoaDon;
    }
}
