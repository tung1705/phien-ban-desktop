package nhom5.model;

import csdl.MyPreparedStatement;
import csdl.TuongTacCSDL;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.HashMap;

import nhom5.util.DateTimeFormat;

public class CuaHang {

	private static HashMap<Integer, SanPham> danhSachSanPham = new HashMap<>();
	private static HashMap<Integer, NhanVienBanHang> danhSachNhanVien = new HashMap<>();
	private static HashMap<Integer, ChiPhi> danhSachChiPhi = new HashMap<>();
	private static HashMap<Integer, HoaDon> danhSachHoaDon = new HashMap<>();
	private static HashMap<Integer, HoaDonLuong> danhSachBangLuong = new HashMap<>();
	
    public static void docDanhSachSanPham() {
        try {
            MyPreparedStatement statement = new MyPreparedStatement("select * from SanPham");
            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
            if (result == null) {
                return;
            }
            while (result.next()) {
                SanPham sp = new SanPham(result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getInt(4),
                        result.getInt(5),
                        result.getInt(6),
                        result.getInt(7),
                        result.getInt(8),
                        result.getString(9),
                        result.getString(12),
                        result.getString(13),
                        result.getString(14),
                        result.getString(15),
                        result.getString(16),
                        result.getInt(17),
                        result.getString(18),
                        result.getString(19),
                        result.getString(20));
                CuaHang.danhSachSanPham.put(new Integer(sp.getId()), sp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return;
        }

    }

    public static void docDanhSachNhanVien() {
        try {
            MyPreparedStatement sql = new MyPreparedStatement("select * from nhanvien");
            ResultSet result = TuongTacCSDL.guiTruyVan(sql);
            if (result == null) {
                return;
            }
            while (result.next()) {
                NhanVienBanHang nv = new NhanVienBanHang(result.getInt(1),
                        result.getString(3),
                        result.getString(2),
                        result.getInt(4),
                        result.getObject(5, LocalDate.class),
                        result.getObject(6, LocalDate.class));
                CuaHang.danhSachNhanVien.put(new Integer(nv.getMa()), nv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return;
        }
    }

    public static void docDanhSachChiPhi() {
        try {
            MyPreparedStatement sql = new MyPreparedStatement("select * from chiphi");
            ResultSet result = TuongTacCSDL.guiTruyVan(sql);
            if (result == null) {
                return;
            }
            while (result.next()) {
                ChiPhi cp = new ChiPhi(result.getInt(1),
                        result.getInt(2),
                        result.getString(3),
                        result.getObject(4, LocalDate.class));
                CuaHang.danhSachChiPhi.put(new Integer(cp.getMaCP()), cp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return;
        }
    }

    public static void docDanhSachHoaDon() {
        try {
            MyPreparedStatement sql = new MyPreparedStatement("select * from hoadon");
            ResultSet result = TuongTacCSDL.guiTruyVan(sql);
            if (result == null) {
                return;
            }
            while (result.next()) {
            	int nhanVienId = result.getInt(3);
            	NhanVienBanHang nv = CuaHang.danhSachNhanVien.get(nhanVienId);
                HoaDon hd = new HoaDon(result.getInt(1),result.getInt(2),nv,result.getObject(4, LocalDate.class));
                CuaHang.danhSachHoaDon.put(new Integer(hd.getId()), hd);

                MyPreparedStatement sm = new MyPreparedStatement("select * from donghoadon where hd_id=?");
                sm.setValue(1, hd.getId());
                ResultSet res = TuongTacCSDL.guiTruyVan(sm);
                if (res != null) {
                    while (res.next()) {
                        int sanPhamId = res.getInt(3);
                        SanPham sp = CuaHang.danhSachSanPham.get(sanPhamId);
                    	DongHoaDon clone = new DongHoaDon(sp,res.getInt(4),res.getInt(5),res.getInt(6));
                        hd.getBanGhi().add(clone);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return;
        }
    }

    public static void docDanhSachBangLuong() {
        try {
            MyPreparedStatement statement = new MyPreparedStatement("select * from hoadonluong");
            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
            if (result == null) {
                return;
            }
            while (result.next()) {

                HoaDonLuong hdl = new HoaDonLuong(result.getInt(1),result.getObject(2, LocalDate.class));

                CuaHang.danhSachBangLuong.put(new Integer(hdl.getId()), hdl);

                MyPreparedStatement sm = new MyPreparedStatement("select * from donghoadonluong where hdl_id=?");
                sm.setValue(1, hdl.getId());
                ResultSet res = TuongTacCSDL.guiTruyVan(sm);

                if (res != null) {
                    while (res.next()) {
                        int nhanVienId = res.getInt(2);
                        NhanVienBanHang nv = CuaHang.danhSachNhanVien.get(nhanVienId);
                    	DongHoaDonLuong clone = new DongHoaDonLuong(nv, res.getInt(4));
                        hdl.getBangLuong().add(clone);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return;
        }
    }
    
    public static HashMap<Integer, SanPham> getDanhSachSanPham()
    {
    	return CuaHang.danhSachSanPham;
    }
    
    public static HashMap<Integer, NhanVienBanHang> getDanhSachNhanVien()
    {
    	return CuaHang.danhSachNhanVien;
    }
    
    public static HashMap<Integer, ChiPhi> getDanhSachChiPhi()
    {
    	return CuaHang.danhSachChiPhi;
    }
    
    public static HashMap<Integer, HoaDon> getDanhSachHoaDon()
    {
    	return CuaHang.danhSachHoaDon;
    }
    
    public static HashMap<Integer, HoaDonLuong> getDanhSachBangLuong()
    {
    	return CuaHang.danhSachBangLuong;
    }
}

//    public static SanPham getSanPham(int id) {
//        SanPham sp = null;
//        try {
//            MyPreparedStatement statement = new MyPreparedStatement("select * from SanPham where id=?");
//            statement.setValue(1, id);
//            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
//            if (result != null && result.next()) {
//            	sp = new SanPham(result.getInt(1),
//                        result.getString(2),
//                        result.getString(3),
//                        result.getInt(4),
//                        result.getInt(5),
//                        result.getInt(6),
//                        result.getInt(7),
//                        result.getInt(8),
//                        result.getString(9),
//                        result.getString(12),
//                        result.getString(13),
//                        result.getString(14),
//                        result.getString(15),
//                        result.getString(16),
//                        result.getInt(17),
//                        result.getString(18),
//                        result.getString(19),
//                        result.getString(20));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            return sp;
//        }
//    }
//
//    public static NhanVienBanHang getNhanVien(int id) {
//        NhanVienBanHang nv = null;
//        try {
//            MyPreparedStatement statement = new MyPreparedStatement("select * from nhanvien where id=?");
//            statement.setValue(1, id);
//            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
//
//            if (result != null & result.next()) {
//            	nv = new NhanVienBanHang(result.getInt(1),
//                        result.getString(2),
//                        result.getString(3),
//                        result.getInt(4),
//                        result.getDate(5),
//                        result.getDate(6));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            return nv;
//        }
//    }
//
//    public static ChiPhi getChiPhi(int id) {
//        ChiPhi cp = null;
//        try {
//            MyPreparedStatement statement = new MyPreparedStatement("select * from chiphi where id=?");
//            statement.setValue(1, id);
//            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
//
//            if (result != null && result.next()) {
//            	cp = new ChiPhi(result.getInt(1),
//                        result.getInt(2),
//                        result.getString(3),
//                        result.getDate(4));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            return cp;
//        }
//    }
//
//    public static HoaDon getHoaDon(int id) {
//        HoaDon hd = null;
//        try {
//            MyPreparedStatement statement = new MyPreparedStatement("select * from hoadon where id=?");
//            statement.setValue(1, id);
//            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
//
//            if (result != null && result.next()) {
//
//            	hd = new HoaDon(result.getInt(1),
//                		result.getInt(2),
//                		result.getInt(3),
//                        result.getDate(4));
//
//                MyPreparedStatement sm = new MyPreparedStatement("select * from donghoadon where hd_id=?");
//                sm.setValue(1, id);
//                ResultSet res = TuongTacCSDL.guiTruyVan(sm);
//
//                if (res != null) {
//                    while (res.next()) {
//                        DongHoaDon clone = new DongHoaDon(
//                        		result.getInt(3),
//                        		result.getInt(4),
//                        		result.getInt(5),
//                        		result.getInt(6));
//                        hd.getBanGhi().add(clone);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            return hd;
//        }
//    }
//
//    public static HoaDonLuong getBangLuong(int id) {
//        HoaDonLuong hdl = null;
//        try {
//            MyPreparedStatement statement = new MyPreparedStatement("select * from hoadonluong where id=?");
//            statement.setValue(1, id);
//            ResultSet result = TuongTacCSDL.guiTruyVan(statement);
//            
//            if (result != null && result.next()) {
//
//            	hdl = new HoaDonLuong(result.getInt(1), result.getDate(2));
//                MyPreparedStatement sm = new MyPreparedStatement("select * from donghoadonluong where hdl_id=?");
//                sm.setValue(1, id);
//                ResultSet result1 = TuongTacCSDL.guiTruyVan(sm);
//
//                if (result1 != null) {
//                    while (result1.next()) {
//                    	DongHoaDonLuong clone = new DongHoaDonLuong(result.getInt(2), result.getInt(4));
//                        hdl.getBangLuong().add(clone);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            return hdl;
//        }
//    }
//}
