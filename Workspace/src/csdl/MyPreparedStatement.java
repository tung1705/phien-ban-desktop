package csdl;

import java.util.ArrayList;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import nhom5.util.DateTimeFormat;

public class MyPreparedStatement {

    private String[] splitted_parts;
    private String[] values;
    private int setted;
    private int question_marks_count;

    public MyPreparedStatement(String sql) {
        
        if(sql.charAt(sql.length()-1) == '?' ){
            sql+=" ";
        }
        
        splitted_parts = sql.split("\\?");
        question_marks_count = splitted_parts.length - 1;
        values = new String[question_marks_count];
        setted = 0;
    }
    
    public void setValue(int index, int value){
         if (index > 0 && index <= question_marks_count) {
            if (values[index - 1] == null) {
                setted++;
            }
            values[index - 1] = value+"";
        }
    }

    public void setValue(int index, String value) {
        if (index > 0 && index <= question_marks_count) {
            if (values[index - 1] == null) {
                setted++;
            }
            values[index - 1] = '"'+value+'"';
        }
    }
    
    public void setValue(int index, Date value) {
        if (index > 0 && index <= question_marks_count) {
            if (values[index - 1] == null) {
                setted++;
            }
            values[index - 1] = '"'+value.toString()+'"';
        }
    }
    
    public void setValue(int index, Timestamp value) {
        if (index > 0 && index <= question_marks_count) {
            if (values[index - 1] == null) {
                setted++;
            }
            values[index - 1] = '"'+value.toString()+'"';
        }
    }
    
    public void setValue(int index, LocalDate value) {
        if (index > 0 && index <= question_marks_count) {
            if (values[index - 1] == null) {
                setted++;
            }
            values[index - 1] = '"'+value.toString()+'"';
        }
    }

    public String toString() {

        String statement = "";

        if (setted == question_marks_count) {
            for (int i = 0; i < question_marks_count; i++) {
                statement += splitted_parts[i] + values[i];

            }
            statement += splitted_parts[question_marks_count];
        }

        return statement;
    }
}
