package nhom5.model;

import java.io.Serializable;

public class SanPham implements Serializable {

    private int id;
    private String loai;
    private String ten;
    private int giaMua;
    private int giaBan;
    private int conLai;
    private int daBan;
    private int likes;
    private String linkanh;
    private String congTyPhatHanh;
    private String nhaXuatBan;
    private String tacGia;
    private String dichGia;
    private String loaiBia;
    private int soTrang;
    private String ngayXuatBan;
    private String sku;
    private String noiDungChinh;
    
	public SanPham(int id, String loai, String ten, int giaMua, int giaBan, int conLai, int daBan, int likes, String linkanh,
			String congTyPhatHanh, String nhaXuatBan, String tacGia, String dichGia, String loaiBia, int soTrang,
			String ngayXuatBan, String sku, String noiDungChinh) {
		super();
		this.id = id;
		this.loai = loai;
		this.ten = ten;
		this.giaMua = giaMua;
		this.giaBan = giaBan;
		this.conLai = conLai;
		this.daBan = daBan;
		this.likes = likes;
		this.linkanh = linkanh;
		this.congTyPhatHanh = congTyPhatHanh;
		this.nhaXuatBan = nhaXuatBan;
		this.tacGia = tacGia;
		this.dichGia = dichGia;
		this.loaiBia = loaiBia;
		this.soTrang = soTrang;
		this.ngayXuatBan = ngayXuatBan;
		this.sku = sku;
		this.noiDungChinh = noiDungChinh;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoai() {
		return loai;
	}
	public void setLoai(String loai) {
		this.loai = loai;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public int getGiaMua() {
		return giaMua;
	}
	public void setGiaMua(int giaMua) {
		this.giaMua = giaMua;
	}
	public int getGiaBan() {
		return giaBan;
	}
	public void setGiaBan(int giaBan) {
		this.giaBan = giaBan;
	}
	public int getConLai() {
		return conLai;
	}
	public void setConLai(int conLai) {
		this.conLai = conLai;
	}
	public int getDaBan() {
		return daBan;
	}
	public void setDaBan(int daBan) {
		this.daBan = daBan;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public String getLinkanh() {
		return linkanh;
	}
	public void setLinkanh(String linkanh) {
		this.linkanh = linkanh;
	}
	public String getCongTyPhatHanh() {
		return congTyPhatHanh;
	}
	public void setCongTyPhatHanh(String congTyPhatHanh) {
		this.congTyPhatHanh = congTyPhatHanh;
	}
	public String getNhaXuatBan() {
		return nhaXuatBan;
	}
	public void setNhaXuatBan(String nhaXuatBan) {
		this.nhaXuatBan = nhaXuatBan;
	}
	public String getTacGia() {
		return tacGia;
	}
	public void setTacGia(String tacGia) {
		this.tacGia = tacGia;
	}
	public String getDichGia() {
		return dichGia;
	}
	public void setDichGia(String dichGia) {
		this.dichGia = dichGia;
	}
	public String getLoaiBia() {
		return loaiBia;
	}
	public void setLoaiBia(String loaiBia) {
		this.loaiBia = loaiBia;
	}
	public int getSoTrang() {
		return soTrang;
	}
	public void setSoTrang(int soTrang) {
		this.soTrang = soTrang;
	}
	public String getNgayXuatBan() {
		return ngayXuatBan;
	}
	public void setNgayXuatBan(String ngayXuatBan) {
		this.ngayXuatBan = ngayXuatBan;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getNoiDungChinh() {
		return noiDungChinh;
	}
	public void setNoiDungChinh(String noiDungChinh) {
		this.noiDungChinh = noiDungChinh;
	}
    
    
}
